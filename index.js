document.addEventListener('DOMContentLoaded', () => {
    let counter = 0;
    document.querySelector('textarea').addEventListener(('change'), (e) => {
        if(e.target.value != '') {
            e.target.closest('form').querySelector('input[type=submit]').style = 'cursor:default';
            e.target.closest('form').querySelector('input[type=submit]').removeAttribute('disabled');
        } else {
            e.target.closest('form').querySelector('input[type=submit]').style = 'cursor:not-allowed';
            e.target.closest('form').querySelector('input[type=submit]').setAttribute('disabled', true);
        }
    })
    document.querySelector('form [type=submit]').addEventListener('click', (e) => {
        e.preventDefault();
        if (!e.target.closest('form').dataset.target) {
            let msg = document.createElement('div');
            msg.classList.add('new-msg');
            msg.dataset.target = `msg${counter += 1}`;
            msg.innerHTML = `
            <p>
                ${e.target.closest('form').querySelector('textarea').value}
            </p>
            <div class='btn-container'>
                <button class='btn-edit'>Editar</button>
                <button class='btn-delete'>Excluir</button>
            </div>`;
            e.target.closest('fieldset').appendChild(msg);
            e.target.closest('form').querySelector('textarea').value = '';
        } else {
            document.querySelector(`.new-msg[data-target=${e.target.closest('form').dataset.target}]`).innerHTML = `
            <p>
                ${e.target.closest('form').querySelector('textarea').value}
            </p>
            <div class='btn-container'>
                <button class='btn-edit'>Editar</button>
                <button class='btn-delete'>Excluir</button>
            </div>`;
            e.target.closest('form').querySelector('textarea').value = '';
            delete e.target.closest('form').dataset['target'];
        }

        document.querySelectorAll('.btn-edit').forEach((el) => {
            el.addEventListener('click', (e) => {
                document.querySelector('form').dataset.target = e.target.closest('[data-target*=msg]').dataset.target;
                document.querySelector('form textarea').value = e.target.closest('.new-msg').querySelector('p').innerText;
            })
        })

        document.querySelectorAll('.btn-delete').forEach((el) => {
            el.addEventListener('click', (e) => {
                e.target.closest('.new-msg').remove();
            })
        })
    })
})